# GitLab Agent for Kubernetes CI image

This project builds container image used in [GitLab Agent for Kubernetes](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent) CI. Container images are pushed to https://hub.docker.com/repository/docker/gitlab/gitlab-agent-ci-image as `gitlab/gitlab-agent-ci-image:latest`.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md).
